import java.io.*;
import java.util.regex.*;

/**
 * Данный класс содержит методы, извлекающие из файла JavaClass.java идентефикаторы.
 * Комментарии игнорируются, лишние пробелы вырезаются, строки в кавычках исключаются.
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class Main {

    public static String PATHTOCLASS = "JavaClass.java";
    public static String OUTPUTFILENAME = "OUTPUT.txt";

    public static void main(String[] args) {
        Matcher matcher = Pattern.compile("([a-z_]\\w*)", Pattern.CASE_INSENSITIVE).matcher("");
        StringBuilder fileContent = new StringBuilder();
        //Сначала читаем содержимое файла, имя которого сохранено в константе PATHTOCLASS.
        //Читаем целиком и полностью.
        fileContent.append(readFile(PATHTOCLASS));
        // Удаляем:
        // * Многострочные комментарии
        fileContent = deleteFragments(fileContent, "/*", "*/");
        // * Однострочные комментарии
        fileContent = deleteFragments(fileContent, "//", "\n");
        // * Строки в кавычках
        fileContent = deleteFragments(fileContent, "\"", "\"");
        int f;
        //А вот тут удаляем лишние пробелы и переносы строк:>
        //========= START =========
        while ((f = fileContent.indexOf("  ")) >= 0) {
            fileContent.replace(f, f+2, " ");
        }
        while ((f = fileContent.indexOf("\n")) >= 0) {
            fileContent.delete(f, f + 1);
        }
        //========== END =========
        //Здесь происходит магия с регулярными выражениями.
        matcher.reset(fileContent);
        StringBuilder result = new StringBuilder();
        while (matcher.find()) {
            result.append(matcher.group() + "\n"); //Пишем построчно
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(OUTPUTFILENAME))) {
            writer.write(result.toString());
            writer.flush(); //На всякий случай выталкиваем буфер в файл.
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Данный метод возвращает содержимое файла fileName в виде строки.
     * @param fileName Имя считываемого файла
     * @return Содержимое файла fileName в виде строки.
     */
    public static String readFile(String fileName) {
        StringBuilder fileContent = new StringBuilder(); //Используем стрингбилдер, чтобы не нагружать машину.
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String s;
            while ((s = reader.readLine()) != null) {
                fileContent.append(s);
                fileContent.append("\n");
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileContent.toString();
    }


    /**
     * Данный метод удаляет фрагменты из строки, хранящейся в объекте inputStr, которые начинаются с последовательности
     * символов startSign и заканчиваются endSign. Обработанная строка возвращается, как результат.
     * @param inputStr Исходный текст в StringBuilder
     * @param startSign Начальная последовательность символов
     * @param endSign Конечная последовательность символов
     * @return Строка с удалёнными фрагментами, которые начинаются с последовательности символов startSign и заканчиваются
     * endSign.
     */
    public static StringBuilder deleteFragments(StringBuilder inputStr, String startSign, String endSign) {
        int i,j;
        while ((i = inputStr.indexOf(startSign)) >= 0 & (j = inputStr.indexOf(endSign, i+1)) > 0)  {
            inputStr.delete(i, j);
        }
        return inputStr; //Не знаю, насколько целесообразно возвращать тот же объект, но пусть будет так.
    }
}
